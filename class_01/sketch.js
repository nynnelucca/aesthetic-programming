function setup() {
  // put setup code here
  createCanvas(640,480);
  print("hello world");
}

function draw() {
  // put drawing code here
  background(0, 250, 0);
  // noCursor();
  //style
  strokeWeight(4);
  stroke(255, 0, 0);
  fill(0, 50, 255);
  ellipse(40, 35, 55);
  line(25, 130, 130, 136);
  //med rundede hjørner
  rect(100, 20, 50, 50, 20);
  text("hello world!", 50, 200);
  textSize(50);
  text(frameCount, width / 2, height / 2);
  //teksten følger musen og skriver dens x+y position
  text("mouseX: "+mouseX, mouseX, mouseY);
  text("mouseY: "+mouseY, mouseX, mouseY+50);
}
