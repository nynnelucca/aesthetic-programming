// 6.2: Classes in JavaScript with ES6 - p5.js Tutorial: https://www.youtube.com/watch?v=T-HGdc8L-7w&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=24
// 6.3:Constructor Arguments with Classes in JavaScript: https://www.youtube.com/watch?v=rHiSsgFRgx4&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=25
//6.4: p5.js Web Editor: Adding JavaScript Files: https://www.youtube.com/watch?v=5nf41qLeagU&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=26

let bubble1; //why let?
let bubble2;

function setup() {
  createCanvas(600, 500);
  bubble1 = new Bubble(200, 200, 40); //creating a new object instance, the actual COOKIE
  bubble2 = new Bubble(500, 400, 20); //arguments that refer to the parameters of constructor function
  print(bubble1.x, bubble1.y);
}

function draw() {
  background(0);
  bubble1.move(); //functions that are part of the object
  bubble1.show();
  bubble2.move();
  bubble2.show();
}

class Bubble { //encapsulation of everything it means to be a certain thing using a 'class' -> can also be thought of as a template / blueprint / COOKIECUTTER
  constructor(tempX, tempY, tempR) { //variables inside constructor = temporary local variables. constructor = a  function that contains data of potential object instance
    this.x = tempX; //data of object
    this.y = tempY;
    this.r = tempR;
    this.col = 0;
  }

  move() { //a function that contains functionality of potential object instance
    this.x = this.x + random (-5, 5);
    this.y = this.y + random (-5, 5);
  }

  show () {
    stroke(255);
    strokeWeight(4);
    fill(this.col);
    ellipse(this.x, this.y, this.r*2); //uses constructor parameters
  }
}
